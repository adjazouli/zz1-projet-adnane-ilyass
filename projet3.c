#define SDL_MAIN_HANDLED
#include <SDL2/SDL.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SCREEN_WIDTH 600
#define SCREEN_HEIGHT 600
#define GRID_SIZE 9
#define SQUARE_SIZE ((SCREEN_WIDTH - (GRID_SIZE - 1) * 2) / GRID_SIZE)
#define SPACING 2
#define CIRCLE_RADIUS 20

typedef struct {
    int row;
    int col;
    SDL_Color color;
    bool active; // Active indicates if the piece is on the board or captured
} Pawn;

SDL_Color playerColors[2] = {{0, 0, 255, 255}, {255, 0, 0, 255}};
int currentPlayer = 0; // 0 for player 1, 1 for player 2

bool needToRespawn[2] = {false, false}; // Flags for each player to track if they need to respawn a piece
int respawnPawnIndex[2] = {-1, -1}; // Indices of the pawns to be respawned

void end_sdl(char ok, char const* msg, SDL_Window* window, SDL_Renderer* renderer) {
    char msg_formated[255];
    int l;

    if (!ok) {
        strncpy(msg_formated, msg, 250);
        l = strlen(msg_formated);
        strcpy(msg_formated + l, " : %s\n");

        SDL_Log(msg_formated, SDL_GetError());
    }

    if (renderer != NULL) {
        SDL_DestroyRenderer(renderer);
        renderer = NULL;
    }
    if (window != NULL) {
        SDL_DestroyWindow(window);
        window = NULL;
    }

    SDL_Quit();

    if (!ok) {
        exit(EXIT_FAILURE);
    }
}

void renderFilledCircle(SDL_Renderer* renderer, int x, int y, int radius, SDL_Color color) {
    SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);

    for (int w = 0; w < radius * 2; w++) {
        for (int h = 0; h < radius * 2; h++) {
            int dx = radius - w; // horizontal offset
            int dy = radius - h; // vertical offset
            if ((dx * dx + dy * dy) <= (radius * radius)) {
                SDL_RenderDrawPoint(renderer, x + dx, y + dy);
            }
        }
    }
}
bool canEliminate(Pawn pawns[], int selectedPawnIndex, int newRow, int newCol) {
    int dx = newRow - pawns[selectedPawnIndex].row;
    int dy = newCol - pawns[selectedPawnIndex].col;

    // Check if the move is diagonal and one step
    if (abs(dx) == 1 && abs(dy) == 1) {
        int targetIndex = -1;
        for (int i = 0; i < 8; ++i) {
            if (i != selectedPawnIndex && pawns[i].row == newRow && pawns[i].col == newCol) {
                targetIndex = i;
                break;
            }
        }

        // Check if there's a pawn of a different color on the target square
        if (targetIndex != -1 && pawns[selectedPawnIndex].color.r != pawns[targetIndex].color.r) {
            return true;
        }
    }
    return false;
}

bool isPathClear(Pawn pawns[], int startRow, int startCol, int endRow, int endCol) {
    int path[81][2] = {
        {8, 0}, {8, 1}, {8, 2}, {8, 3}, {8, 4}, {8, 5}, {8, 6}, {8, 7}, {8, 8},
        {7, 8}, {6, 8}, {5, 8}, {4, 8}, {3, 8}, {2, 8}, {1, 8}, {0, 8}, {0, 7},
        {0, 6}, {0, 5}, {0, 4}, {0, 3}, {0, 2}, {0, 1}, {0, 0}, {1, 0}, {2, 0},
        {3, 0}, {4, 0}, {5, 0}, {6, 0}, {7, 0}, {7, 1}, {7, 2}, {7, 3}, {7, 4},
        {7, 5}, {7, 6}, {7, 7}, {6, 7}, {5, 7}, {4, 7}, {3, 7}, {2, 7}, {1, 7},
        {1, 6}, {1, 5}, {1, 4}, {1, 3}, {1, 2}, {1, 1}, {2, 1}, {3, 1}, {4, 1},
        {5, 1}, {6, 1}, {6, 2}, {6, 3}, {6, 4}, {6, 5}, {6, 6}, {5, 6}, {4, 6},
        {3, 6}, {2, 6}, {2, 5}, {2, 4}, {2, 3}, {2, 2}, {3, 2}, {4, 2}, {5, 2},
        {5, 3}, {5, 4}, {5, 5}, {4, 5}, {3, 5}, {3, 4}, {3, 3}, {4, 3}, {4, 4}
    };

    int startIndex = -1, endIndex = -1;
    for (int i = 0; i < 81; ++i) {
        if (path[i][0] == startRow && path[i][1] == startCol) {
            startIndex = i;
        }
        if (path[i][0] == endRow && path[i][1] == endCol) {
            endIndex = i;
        }
    }

    if (startIndex == -1 || endIndex == -1 || endIndex <= startIndex) {
        return false;
    }

    // Traverse the path from startIndex to endIndex
    for (int i = startIndex + 1; i <= endIndex; ++i) {
        int row = path[i][0];
        int col = path[i][1];
        for (int j = 0; j < 8; ++j) {
            if (pawns[j].row == row && pawns[j].col == col) {
                return false; // Path is blocked
            }
        }
    }

    return true; // Path is clear
}


bool canCapture(Pawn pawns[], int selectedPawnIndex, int newRow, int newCol) {
    int dx = newRow - pawns[selectedPawnIndex].row;
    int dy = newCol - pawns[selectedPawnIndex].col;

    // Check if the move is vertical, horizontal, or diagonal
    if (dx == 0 || dy == 0 || abs(dx) == abs(dy)) {
        for (int i = 0; i < 8; ++i) {
            if (i != selectedPawnIndex && pawns[i].row == newRow && pawns[i].col == newCol &&
                pawns[selectedPawnIndex].color.r != pawns[i].color.r) {
                int opponent = (pawns[i].color.r == playerColors[0].r) ? 0 : 1;
                needToRespawn[opponent] = true;
                respawnPawnIndex[opponent] = i;
                return true;
            }
        }
    }
    return false;
}


bool checkVictory(Pawn pawns[], int *winner) {
    for (int i = 0; i < 8; ++i) {
        if (pawns[i].row == 4 && pawns[i].col == 4) { // Assuming center is at (4, 4)
            *winner = (pawns[i].color.r == playerColors[0].r) ? 0 : 1;
            return true;
        }
    }
    return false;
}

int endGame(int winner) {
    printf("Player %d wins!\n", winner + 1);
    return SDL_FALSE;
}


bool isSpiralPath(int startRow, int startCol, int endRow, int endCol) {
    int path[81][2] = {
        {8, 0}, {8, 1}, {8, 2}, {8, 3}, {8, 4}, {8, 5}, {8, 6}, {8, 7}, {8, 8},
        {7, 8}, {6, 8}, {5, 8}, {4, 8}, {3, 8}, {2, 8}, {1, 8}, {0, 8}, {0, 7},
        {0, 6}, {0, 5}, {0, 4}, {0, 3}, {0, 2}, {0, 1}, {0, 0}, {1, 0}, {2, 0},
        {3, 0}, {4, 0}, {5, 0}, {6, 0}, {7, 0}, {7, 1}, {7, 2}, {7, 3}, {7, 4},
        {7, 5}, {7, 6}, {7, 7}, {6, 7}, {5, 7}, {4, 7}, {3, 7}, {2, 7}, {1, 7},
        {1, 6}, {1, 5}, {1, 4}, {1, 3}, {1, 2}, {1, 1}, {2, 1}, {3, 1}, {4, 1},
        {5, 1}, {6, 1}, {6, 2}, {6, 3}, {6, 4}, {6, 5}, {6, 6}, {5, 6}, {4, 6},
        {3, 6}, {2, 6}, {2, 5}, {2, 4}, {2, 3}, {2, 2}, {3, 2}, {4, 2}, {5, 2},
        {5, 3}, {5, 4}, {5, 5}, {4, 5}, {3, 5}, {3, 4}, {3, 3}, {4, 3}, {4, 4}
    };

    int startIndex = -1, endIndex = -1;
    for (int i = 0; i < 81; ++i) {
        if (path[i][0] == startRow && path[i][1] == startCol) {
            startIndex = i;
        }
        if (path[i][0] == endRow && path[i][1] == endCol) {
            endIndex = i;
        }
    }

    if (startIndex == -1 || endIndex == -1 || endIndex <= startIndex) {
        return false;
    }

    int turnCount = 0;
    int lastDirection = -1;
    for (int i = startIndex + 1; i <= endIndex; ++i) {
        int dx = path[i][0] - path[i - 1][0];
        int dy = path[i][1] - path[i - 1][1];
        int direction = (dx != 0) ? (dx > 0 ? 0 : 1) : (dy > 0 ? 2 : 3);

        if (lastDirection != -1 && lastDirection != direction) {
            ++turnCount;
        }
        lastDirection = direction;

        if (turnCount > 1) {
            return false;
        }
    }

    return true;
}

void drawSpiralPath(SDL_Renderer* renderer) {
    SDL_Rect topRect = {0, 523, 528, 8};
    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
    SDL_RenderFillRect(renderer, &topRect);

    SDL_Rect topRect1 = {523, 63, 8, 467};
    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
    SDL_RenderFillRect(renderer, &topRect1);
    SDL_Rect topRect2 = {63, 63, 467, 8};
    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
    SDL_RenderFillRect(renderer, &topRect2);
    SDL_Rect topRect3 = {63, 63, 8, 404};
    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
    SDL_RenderFillRect(renderer, &topRect3);
    SDL_Rect topRect4 = {63, 459, 404, 8};
    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
    SDL_RenderFillRect(renderer, &topRect4);
    SDL_Rect topRect5 = {459, 126, 8, 404-63};
    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
    SDL_RenderFillRect(renderer, &topRect5);
    SDL_Rect topRect6 = {126, 126, 404-63, 8};
    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
    SDL_RenderFillRect(renderer, &topRect6);
    SDL_Rect topRect7 = {126, 126, 8, 404-140};
    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
    SDL_RenderFillRect(renderer, &topRect7);
    SDL_Rect topRect8 = {126, 390, 274, 8};
    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
    SDL_RenderFillRect(renderer, &topRect8);
    SDL_Rect topRect9 = {392, 192, 8, 208};
    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
    SDL_RenderFillRect(renderer, &topRect9);
    SDL_Rect topRect10 = {192, 192, 208, 8};
    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
    SDL_RenderFillRect(renderer, &topRect10);
    SDL_Rect topRect11 = {192, 192, 8, 140};
    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
    SDL_RenderFillRect(renderer, &topRect11);
    SDL_Rect topRect12 = {192, 327 ,140, 8};
    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
    SDL_RenderFillRect(renderer, &topRect12);
    SDL_Rect topRect13 = {324, 264 ,8, 64};
    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
    SDL_RenderFillRect(renderer, &topRect13);
    SDL_Rect topRect14 = {260, 260,71, 8};
    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
    SDL_RenderFillRect(renderer, &topRect14);
}

int main(int argc, char** argv) {
    (void)argc;
    (void)argv;

    SDL_Window* window = NULL;
    SDL_Renderer* renderer = NULL;
    SDL_bool program_on = SDL_TRUE;
    SDL_Event event;
    Pawn pawns[8] = {
        {8, 0, {255, 0, 0, 255}, true}, {8, 1, {0, 0, 255, 255}, true}, {8, 2, {255, 0, 0, 255}, true}, {8, 3, {0, 0, 255, 255}, true},
        {8, 4, {255, 0, 0, 255}, true}, {8, 5, {0, 0, 255, 255}, true}, {8, 6, {255, 0, 0, 255}, true}, {8, 7, {0, 0, 255, 255}, true}
    };
    int selectedPawnIndex = -1;

    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        end_sdl(0, "ERROR SDL INIT", window, renderer);
    }

    window = SDL_CreateWindow("RITSAMI", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, SCREEN_WIDTH, SCREEN_HEIGHT, 0);
    if (window == NULL) {
        end_sdl(0, "ERROR WINDOW CREATION", window, renderer);
    }

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == NULL) {
        end_sdl(0, "ERROR RENDERER CREATION", window, renderer);
    }

    while (program_on) {
        while (SDL_PollEvent(&event)) {
            switch (event.type) {
                case SDL_QUIT:
                    program_on = SDL_FALSE;
                    break;
                case SDL_MOUSEBUTTONDOWN:
                    if (event.button.button == SDL_BUTTON_LEFT) {
                        int mouseX = event.button.x;
                        int mouseY = event.button.y;
                        int clickedRow = mouseY / (SQUARE_SIZE + SPACING);
                        int clickedCol = mouseX / (SQUARE_SIZE + SPACING);

                        for (int k = 0; k < 8; ++k) {
                            int centerX = pawns[k].col * (SQUARE_SIZE + SPACING) + SQUARE_SIZE / 2;
                            int centerY = pawns[k].row * (SQUARE_SIZE + SPACING) + SQUARE_SIZE / 2;
                            int dx = centerX - mouseX;
                            int dy = centerY - mouseY;
                            if (dx * dx + dy * dy <= CIRCLE_RADIUS * CIRCLE_RADIUS) {
                                if (pawns[k].color.r == playerColors[currentPlayer].r &&  // Check if the clicked pawn is of the current player
                                    pawns[k].color.g == playerColors[currentPlayer].g && 
                                    pawns[k].color.b == playerColors[currentPlayer].b) {
                                    selectedPawnIndex = k;
                                } else {
                                    SDL_Log("It's not your turn!");
                                }
                                break;
                            }
                        }
                    }
                    break;
                    case SDL_MOUSEBUTTONUP:
                        if (event.button.button == SDL_BUTTON_LEFT) {
                            int newRow = event.button.y / (SQUARE_SIZE + SPACING);
                            int newCol = event.button.x / (SQUARE_SIZE + SPACING);

                            if (needToRespawn[currentPlayer]) {
                                // Respawn the captured piece
                                if (newRow == 8 && newCol >= 0 && newCol <= 7) {
                                    int index = respawnPawnIndex[currentPlayer];
                                    pawns[index].row = newRow;
                                    pawns[index].col = newCol;
                                    pawns[index].active = true; // Mark the pawn as active
                                    needToRespawn[currentPlayer] = false; // Clear the respawn flag
                                    currentPlayer = 1 - currentPlayer; // Switch turn to the next player
                                } else {
                                    printf("Invalid respawn position!\n");
                                }
                            } else if (selectedPawnIndex != -1) {
                                if (canCapture(pawns, selectedPawnIndex, newRow, newCol)) {
                                        for (int i = 0; i < 8; ++i) {
                                            if (pawns[i].row == newRow && pawns[i].col == newCol) {
                                                // Capture the opponent's piece
                                                pawns[i].row = -1;
                                                pawns[i].col = -1;
                                                pawns[i].active = false; // Mark the pawn as inactive
                                                break;
                                            }
                                        }
                                        // Move the capturing piece to the new position
                                        pawns[selectedPawnIndex].row = newRow;
                                        pawns[selectedPawnIndex].col = newCol;
                                        currentPlayer = 1 - currentPlayer; // Switch turn to the next player
                                } else if (isSpiralPath(pawns[selectedPawnIndex].row, pawns[selectedPawnIndex].col, newRow, newCol)) {
                                    if (isPathClear(pawns, pawns[selectedPawnIndex].row, pawns[selectedPawnIndex].col, newRow, newCol)) {
                                        // Regular move
                                        pawns[selectedPawnIndex].row = newRow;
                                        pawns[selectedPawnIndex].col = newCol;
                                        int winner;
                                        if (checkVictory(pawns, &winner)) {
                                            program_on = endGame(winner);
                                        }
                                        currentPlayer = 1 - currentPlayer; // Switch turn to the next player
                                    } else {
                                        printf("Invalid move!\n");
                                    }
                                } else {
                                    printf("Invalid move!\n");
                                }
                                selectedPawnIndex = -1;
                            }
                        }
                        break;
                    break;
            }
        }

        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        SDL_RenderClear(renderer);

        for (int i = 0; i < GRID_SIZE; ++i) {
            for (int j = 0; j < GRID_SIZE; ++j) {
                SDL_Rect square = { j * (SQUARE_SIZE + SPACING), i * (SQUARE_SIZE + SPACING), SQUARE_SIZE, SQUARE_SIZE };

                if ((i + j) % 2 == 0) {
                    SDL_SetRenderDrawColor(renderer, 255, 255, 0, 255);
                } else {
                    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
                }

                SDL_RenderFillRect(renderer, &square);
            }
        }

        drawSpiralPath(renderer);

        for (int k = 0; k < 8; ++k) {
            int centerX = pawns[k].col * (SQUARE_SIZE + SPACING) + SQUARE_SIZE / 2;
            int centerY = pawns[k].row * (SQUARE_SIZE + SPACING) + SQUARE_SIZE / 2;
            renderFilledCircle(renderer, centerX, centerY, CIRCLE_RADIUS, pawns[k].color);
        }
                for (int k = 0; k < 8; ++k) {
            if (pawns[k].row != -1 && pawns[k].col != -1) { // Check if pawn is active
                int centerX = pawns[k].col * (SQUARE_SIZE + SPACING) + SQUARE_SIZE / 2;
                int centerY = pawns[k].row * (SQUARE_SIZE + SPACING) + SQUARE_SIZE / 2;
                renderFilledCircle(renderer, centerX, centerY, CIRCLE_RADIUS, pawns[k].color);
            }
        }


        SDL_RenderPresent(renderer);
    }

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}