#include <SDL2/SDL.h>
#include <stdio.h>
#include <SDL2/SDL_image.h>


SDL_Texture* load_texture_from_image(char * file_image_name, SDL_Window *window, SDL_Renderer *renderer ){

 SDL_Surface *my_image = NULL; 
 SDL_Texture *my_texture = NULL; 
 my_image = IMG_Load(file_image_name); 
 if (my_image == NULL)
 fprintf(stderr,"Error");
 my_texture = SDL_CreateTextureFromSurface(renderer, my_image); 
 SDL_FreeSurface(my_image); 
 if (my_texture == NULL) 
 fprintf(stderr,"Error");
 return my_texture;
}
void animation(SDL_Texture* text, SDL_Window* fen, SDL_Renderer* renderer){
 
 SDL_Rect win={0},bg={0};
 SDL_QueryTexture(text,NULL,NULL,&bg.w,&bg.h);
 static int off = 0;
 if (off + 700 <= 0) {
 off = 0;
 }
 off=off -5;
 SDL_Rect rect1 = {off, 0, 800, 600};
 SDL_Rect rect2 = {off+800, 0, 800, 600};
 SDL_Rect source={0,0,800,600};
 SDL_RenderCopy(renderer, text, &source, &rect1);
 SDL_RenderCopy(renderer, text, &source, &rect2);
 SDL_RenderPresent(renderer);
}

int main(int argc, char* argv[]){

 if(SDL_Init(SDL_INIT_VIDEO)!=0){
 fprintf(stderr,"Error %s",SDL_GetError());
 return 1;
 }

 SDL_bool run= SDL_TRUE;
 SDL_Window *fen=SDL_CreateWindow("Animation", SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED,900,600,0);
 SDL_Renderer *renderer=SDL_CreateRenderer(fen,-1,SDL_RENDERER_PRESENTVSYNC | SDL_RENDERER_ACCELERATED);
 SDL_Texture *texture=load_texture_from_image("background.bmp",fen,renderer);
 SDL_Event event;
 SDL_Texture *char_text[40];
 char filename[50];
 for (int i = 1; i < 11; i++) {
sprintf(filename, "%dA.bmp", i);
sprintf(filename, "%dB.bmp", i);
sprintf(filename, "%dC.bmp", i);
sprintf(filename, "%dD.bmp", i);
if(i==8){
    i +=2;
}
 char_text[i] = load_texture_from_image(filename, fen, renderer);
 }
 while (run) {
 while (SDL_PollEvent(&event)) {
 if (event.type == SDL_QUIT) {
 run = SDL_FALSE;
 }
 } 

 SDL_RenderClear(renderer);
 animation(texture,fen, renderer);
 
 SDL_Delay(50); 
 }

 return 0;
}

