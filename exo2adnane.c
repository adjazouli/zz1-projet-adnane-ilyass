#include <SDL2/SDL.h>
#include <stdio.h>
#include <SDL2/SDL_image.h>


SDL_Texture* load_texture_from_image(char * file_image_name, SDL_Window *window, SDL_Renderer *renderer ){

 SDL_Surface *my_image = NULL; 
 SDL_Texture *my_texture = NULL; 
 my_image = IMG_Load(file_image_name); 
 if (my_image == NULL)
 fprintf(stderr,"Error");
 my_texture = SDL_CreateTextureFromSurface(renderer, my_image); 
 SDL_FreeSurface(my_image); 
 if (my_texture == NULL) 
 fprintf(stderr,"Error");
 return my_texture;
}
void animation(SDL_Texture* text, SDL_Texture* char_text[], SDL_Window* fen, SDL_Renderer* renderer, int fps){
 
 SDL_Rect win={0},bg={0};

 SDL_QueryTexture(text,NULL,NULL,&bg.w,&bg.h);
 SDL_GetWindowSize(fen,&win.w,&win.h);

static int off = 0;
 if (off + bg.w <= 0) {
 off = 0;
 }
 off=off -5;

 SDL_Rect rect1 = {off, 0, bg.w, bg.h};
 SDL_Rect rect2 = {off+bg.w, 0, bg.w, bg.h};

 SDL_Rect source={0,0,bg.w,bg.h};

 SDL_RenderCopy(renderer, text, &source, &rect1);
 SDL_RenderCopy(renderer, text, &source, &rect2);
 SDL_Rect pos= {win.w/2 - 50, win.h/2 + 70 , 100, 100};
 SDL_RenderCopy(renderer, char_text[fps], NULL, &pos);
 SDL_RenderPresent(renderer);
}

int main(int argc, char* argv[]){

 if(SDL_Init(SDL_INIT_VIDEO)!=0){
 fprintf(stderr,"Error %s",SDL_GetError());
 return 1;
 }

 SDL_bool run= SDL_TRUE;
 SDL_Window *fen=SDL_CreateWindow("Animation", SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED,600,400,SDL_WINDOW_RESIZABLE);
 SDL_Renderer *renderer=SDL_CreateRenderer(fen,-1,SDL_RENDERER_PRESENTVSYNC | SDL_RENDERER_ACCELERATED);
 SDL_Texture *texture=load_texture_from_image("background.bmp",fen,renderer);
 SDL_Event event;
 SDL_Texture *char_text[8];
 char filename[50];

 for (int i = 1; i < 8; i++) {
 sprintf(filename, "Run%d.bmp", i);
 char_text[i] = load_texture_from_image(filename, fen, renderer);
 }
 int frame=0;
 while (run) {
 while (SDL_PollEvent(&event)) {
 if (event.type == SDL_QUIT) {
 run = SDL_FALSE;
 }
 } 

 SDL_RenderClear(renderer);
 animation(texture, char_text, fen, renderer, frame);
 frame = (frame + 1) % 7;
 SDL_Delay(50); 
 }

 return 0;
}

