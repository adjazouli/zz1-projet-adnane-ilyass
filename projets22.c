#include <SDL2/SDL.h>
#include <stdio.h>
#include <SDL2/SDL_image.h>
#include <time.h>
#include <stdlib.h>
#include<stdbool.h>
#include <string.h>

typedef struct {
    int numero;
    const char* type;
    SDL_Texture* texture;
} Carte;
typedef struct {
    Carte carte;
    SDL_Rect rect;
} CarteMilieu;
SDL_Texture* load_texture_from_image(const char* file_image_name, SDL_Renderer* renderer) {
    SDL_Surface* my_image = IMG_Load(file_image_name);
    if (my_image == NULL) {
        fprintf(stderr, "Error loading image %s: %s\n", file_image_name, IMG_GetError());
        return NULL;
    }
    SDL_Texture* my_texture = SDL_CreateTextureFromSurface(renderer, my_image);
    SDL_FreeSurface(my_image);
    if (my_texture == NULL) {
        fprintf(stderr, "Error creating texture from %s: %s\n", file_image_name, SDL_GetError());
    }
    return my_texture;
}

void init_jeu_de_cartes(Carte* jeu_de_cartes, SDL_Renderer* renderer) {
    const char* types[] = {"A", "B", "C", "D"};
    char filename[50];
    int index = 0;
    for (int t = 0; t < 4; ++t) {
        for (int n = 1; n <= 7; ++n) {
    
            jeu_de_cartes[index].numero = n;
            jeu_de_cartes[index].type = types[t];
            sprintf(filename, "%d%s.bmp", n, types[t]);
            jeu_de_cartes[index].texture = load_texture_from_image(filename, renderer);
            ++index; 
        }
        for (int n = 10; n <= 12; ++n)
        {
            
            jeu_de_cartes[index].numero = n;
            jeu_de_cartes[index].type = types[t];
            sprintf(filename, "%d%s.bmp", n, types[t]);
            jeu_de_cartes[index].texture = load_texture_from_image(filename, renderer);
            ++index;
        }
    }
}

void melanger_cartes(Carte* cartes, int n) {
    srand(time(NULL));
    for (int i = 0; i < n - 1; ++i) {
        int j = i + rand() / (RAND_MAX / (n - i) + 1);
        Carte temp = cartes[i];
        cartes[i] = cartes[j];
        cartes[j] = temp;
    }
}
void tirer_quatre_cartes(Carte* main, Carte* jeu_de_cartes) {
    melanger_cartes(jeu_de_cartes, 40);
    for (int i = 0; i < 4; ++i) {
        main[i] = jeu_de_cartes[i];
    }
}
void animation(SDL_Texture* text, SDL_Window* fen, SDL_Renderer* renderer) {
    SDL_Rect win = { 0 }, bg = { 0 };
    SDL_QueryTexture(text, NULL, NULL, &bg.w, &bg.h);
    SDL_GetWindowSize(fen, &win.w, &win.h);

    static int off = 0;
    if (off + 700 <= 0) {
        off = 0;
    }
    off -= 10;

    SDL_Rect rect1 = { off, 0, 800, 600 };
    SDL_Rect rect2 = { off + 800, 0, 800, 600 };
    SDL_Rect source = { 0, 0, bg.w, bg.h };

    SDL_RenderCopy(renderer, text, &source, &rect1);
    SDL_RenderCopy(renderer, text, &source, &rect2);
}

void render_rectangles(SDL_Renderer* renderer, SDL_Rect* rectangles, Carte* main) {
    for (int i = 0; i < 4; ++i) {
        SDL_RenderCopy(renderer, main[i].texture, NULL, &rectangles[i]);
    }
}

bool est_carte_tiree(Carte* main, Carte carte) {
    for (int i = 0; i < 4; ++i) {
        if (main[i].numero == carte.numero && strcmp(main[i].type, carte.type) == 0) {
            return true;
        }
    }
    return false;
}

void choisir_carte_milieu(CarteMilieu* carteMilieu, Carte* jeu_de_cartes, Carte* main) {
    int index;
    do {
        index = rand() % 40;
    } while (est_carte_tiree(main, jeu_de_cartes[index]));
    carteMilieu->carte = jeu_de_cartes[index];
    carteMilieu->rect.x = 400;
    carteMilieu->rect.y = 250;
    carteMilieu->rect.w = 100;
    carteMilieu->rect.h = 150;
}


int main(int argc, char* argv[]) {
    (void ) argc;
    (void) argv;
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        fprintf(stderr, "Error initializing SDL: %s\n", SDL_GetError());
        return 1;
    }

    SDL_Window* fen = SDL_CreateWindow("Animation", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 900, 700, 0);
    if (!fen) {
        fprintf(stderr, "Error creating window: %s\n", SDL_GetError());
        SDL_Quit();
        return 1;
    }

    SDL_Renderer* renderer = SDL_CreateRenderer(fen, -1, SDL_RENDERER_PRESENTVSYNC | SDL_RENDERER_ACCELERATED);
    if (!renderer) {
        fprintf(stderr, "Error creating renderer: %s\n", SDL_GetError());
        SDL_DestroyWindow(fen);
        SDL_Quit();
        return 1;
    }

    SDL_Texture* background_texture = load_texture_from_image("background.bmp", renderer);
    if (!background_texture) {
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(fen);
        SDL_Quit();
        return 1;
    }

    Carte jeu_de_cartes[40];
    init_jeu_de_cartes(jeu_de_cartes, renderer);

    Carte main[4];
    tirer_quatre_cartes(main, jeu_de_cartes);

    CarteMilieu carteMilieu;
    choisir_carte_milieu(&carteMilieu, jeu_de_cartes, main);

    SDL_Rect rectangles[4];
    int rect_width = 100;
    int rect_height = 150;
    int spacing = 5;
    int total_width = 4 * rect_width + 3 * spacing;
    int start_x = (900 - total_width) / 2;
    int y_position = 640 - rect_height ;

    for (int i = 0; i < 4; ++i) {
        rectangles[i].x = start_x + i * (rect_width + spacing);
        rectangles[i].y = y_position ;
        rectangles[i].w = rect_width;
        rectangles[i].h = rect_height;
    }
    
    int carteEnDeplacement = -1;
    SDL_Rect anciennePosition;

    SDL_Event event;
    SDL_bool run = SDL_TRUE;
    while (run) {
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) {
                run = SDL_FALSE;
            } else if (event.type == SDL_MOUSEBUTTONDOWN) {
                int mouseX, mouseY;
                SDL_GetMouseState(&mouseX, &mouseY);
                for (int i = 0; i < 4; ++i) {
                    if (mouseX >= rectangles[i].x && mouseX <= rectangles[i].x + rectangles[i].w &&
                        mouseY >= rectangles[i].y && mouseY <= rectangles[i].y + rectangles[i].h) {
                        carteEnDeplacement = i;
                        anciennePosition = rectangles[i];
                        break;
                    }
                }
            } else if (event.type == SDL_MOUSEBUTTONUP && carteEnDeplacement != -1) {
                int mouseX, mouseY;
                SDL_GetMouseState(&mouseX, &mouseY);
                if (mouseX >= carteMilieu.rect.x && mouseX <= carteMilieu.rect.x + carteMilieu.rect.w &&
                    mouseY >= carteMilieu.rect.y && mouseY <= carteMilieu.rect.y + carteMilieu.rect.h) {
                    carteMilieu.carte = main[carteEnDeplacement];
                    // RÃ©initialiser la position de la carte dÃ©placÃ©e dans la main
                    rectangles[carteEnDeplacement] = anciennePosition;
                } else {
                    // Remettre la carte dÃ©placÃ©e Ã  sa position d'origine
                    rectangles[carteEnDeplacement] = anciennePosition;
                }
                carteEnDeplacement = -1;
            } else if (event.type == SDL_MOUSEMOTION && carteEnDeplacement != -1) {
                int mouseX, mouseY;
                SDL_GetMouseState(&mouseX, &mouseY);
                rectangles[carteEnDeplacement].x = mouseX - rect_width / 2;
                rectangles[carteEnDeplacement].y = mouseY - rect_height / 2;
            }
        }

        SDL_RenderClear(renderer);

        animation(background_texture, fen, renderer);

        SDL_RenderCopy(renderer, carteMilieu.carte.texture, NULL, &carteMilieu.rect);

        render_rectangles(renderer, rectangles, main);

        SDL_RenderPresent(renderer);

        SDL_Delay(50);
    }

    for (int i = 0; i < 40; ++i) {
        SDL_DestroyTexture(jeu_de_cartes[i].texture);
    }
    SDL_DestroyTexture(background_texture);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(fen);
    SDL_Quit();

    return 0;
}