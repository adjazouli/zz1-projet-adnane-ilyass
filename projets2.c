#include <SDL2/SDL.h>
#include <stdio.h>
#include <SDL2/SDL_image.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    int numero;
    const char* type;
    SDL_Texture* texture;
} Carte;

SDL_Texture* load_texture_from_image(const char* file_image_name, SDL_Renderer* renderer) {
    SDL_Surface* my_image = IMG_Load(file_image_name);
    if (my_image == NULL) {
        fprintf(stderr, "Error loading image %s: %s\n", file_image_name, IMG_GetError());
        return NULL;
    }
    SDL_Texture* my_texture = SDL_CreateTextureFromSurface(renderer, my_image);
    SDL_FreeSurface(my_image);
    if (my_texture == NULL) {
        fprintf(stderr, "Error creating texture from %s: %s\n", file_image_name, SDL_GetError());
    }
    return my_texture;
}

void drawTransparentButton(SDL_Renderer* renderer, SDL_Rect rect) {
    SDL_SetRenderDrawColor(renderer, 250, 0, 0, 0); // Transparent
    SDL_RenderFillRect(renderer, &rect);
    // (Vous pouvez ajouter une icône ou du texte ici si vous le souhaitez)
}

void init_jeu_de_cartes(Carte* jeu_de_cartes, SDL_Renderer* renderer) {
    const char* types[] = {"A", "B", "C", "D"};
    char filename[50];
    int index = 0;
    for (int t = 0; t < 4; ++t) {
        for (int n = 1; n <= 7; ++n) {
            jeu_de_cartes[index].numero = n;
            jeu_de_cartes[index].type = types[t];
            sprintf(filename, "%d%s.bmp", n, types[t]);
            jeu_de_cartes[index].texture = load_texture_from_image(filename, renderer);
            ++index;
        }
        for (int n = 10; n <= 12; ++n) {
            jeu_de_cartes[index].numero = n;
            jeu_de_cartes[index].type = types[t];
            sprintf(filename, "%d%s.bmp", n, types[t]);
            jeu_de_cartes[index].texture = load_texture_from_image(filename, renderer);
            ++index;
        }
    }
}

int findEmptySlot(Carte* main) {
    for (int i = 0; i < 7; ++i) {
        if (main[i].texture == NULL) {
            return i;
        }
    }
    return -1; // Aucun emplacement vide trouvé
}

int carte_presente_dans_main(Carte carte, Carte* main) {
    for (int i = 0; i < 7; ++i) {
        if (main[i].texture != NULL && carte.numero == main[i].numero && strcmp(carte.type, main[i].type) == 0) {
            return 1;
        }
    }
    return 0;
}

void melanger_cartes(Carte* cartes, int n) {
    srand(time(NULL));
    for (int i = 0; i < n - 1; ++i) {
        int j = i + rand() / (RAND_MAX / (n - i) + 1);
        Carte temp = cartes[i];
        cartes[i] = cartes[j];
        cartes[j] = temp;
    }
}

void tirer_quatre_cartes(Carte* main, Carte* jeu_de_cartes) {
    melanger_cartes(jeu_de_cartes, 40);
    for (int i = 0; i < 4; ++i) {
        main[i] = jeu_de_cartes[i];
    }
}

Carte tirer_carte_au_milieu(Carte* jeu_de_cartes, Carte* main) {
    int index;
    do {
        index = rand() % 40; // Choix aléatoire parmi toutes les cartes
    } while (carte_presente_dans_main(jeu_de_cartes[index], main)); // Vérifie si la carte est déjà dans la main

    return jeu_de_cartes[index];
}

void animation(SDL_Texture* text, SDL_Window* fen, SDL_Renderer* renderer) {
    SDL_Rect win = { 0 }, bg = { 0 };
    SDL_QueryTexture(text, NULL, NULL, &bg.w, &bg.h);
    SDL_GetWindowSize(fen, &win.w, &win.h);

    static int off = 0;
    if (off + 700 <= 0) {
        off = 0;
    }
    off -= 10;

    SDL_Rect rect1 = { off, 0, 800, 600 };
    SDL_Rect rect2 = { off + 800, 0, 800, 600 };
    SDL_Rect source = { 0, 0, bg.w, bg.h };

    SDL_RenderCopy(renderer, text, &source, &rect1);
    SDL_RenderCopy(renderer, text, &source, &rect2);
}

void render_rectangles(SDL_Renderer* renderer, SDL_Rect* rectangles, Carte* main, SDL_Rect* middle_rect, Carte middle_card) {
    for (int i = 0; i < 7; ++i) {
        if (main[i].texture != NULL) {
            SDL_RenderCopy(renderer, main[i].texture, NULL, &rectangles[i]);
        }
    }
    SDL_RenderCopy(renderer, middle_card.texture, NULL, middle_rect);
}

void show_invalid_move_message(SDL_Renderer* renderer) {
    SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Invalid Move", "You can only play a card with the same type or number!", NULL);
}

void centrer_cartes(SDL_Rect* rectangles, int nb_cartes, int largeur_fenetre, int rect_width, int rect_height, int spacing) {
    int total_width = nb_cartes * rect_width + (nb_cartes - 1) * 
    spacing;
    int start_x = (largeur_fenetre - total_width) / 2;
    for (int i = 0; i < nb_cartes; ++i) {
        rectangles[i].x = start_x + i * (rect_width + spacing);
        rectangles[i].y = 700 - rect_height - 110;
        rectangles[i].w = rect_width;
        rectangles[i].h = rect_height;
    }
}

void render_player_hand(SDL_Renderer* renderer, SDL_Rect* player_hand_rect, int num_cards) {
    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255); // Red color for the hidden hand rectangle
    SDL_RenderFillRect(renderer, player_hand_rect);
    
    // Display the number of cards in the player's hand
    char text[5];
    sprintf(text, "%d", num_cards);
    // Assuming you have a function to render text. For example:
    // renderText(renderer, text, player_hand_rect->x + player_hand_rect->w / 2, player_hand_rect->y + player_hand_rect->h / 2);
}

int main(int argc, char* argv[]) {
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        fprintf(stderr, "Error initializing SDL: %s\n", SDL_GetError());
        return 1;
    }

    SDL_Window* fen = SDL_CreateWindow("Hezz2", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 900, 600, 0);
    if (!fen) {
        fprintf(stderr, "Error creating window: %s\n", SDL_GetError());
        SDL_Quit();
        return 1;
    }

    SDL_Renderer* renderer = SDL_CreateRenderer(fen, -1, SDL_RENDERER_PRESENTVSYNC | SDL_RENDERER_ACCELERATED);
    if (!renderer) {
        fprintf(stderr, "Error creating renderer: %s\n", SDL_GetError());
        SDL_DestroyWindow(fen);
        SDL_Quit();
        return 1;
    }

    SDL_Texture* background_texture = load_texture_from_image("background.bmp", renderer);
    if (!background_texture) {
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(fen);
        SDL_Quit();
        return 1;
    }

    SDL_Rect buttonRect = {820, 10, 60, 40};
    Carte lastDrawnCard = {0};
    int premierCoup = 1;

    Carte jeu_de_cartes[40];
    init_jeu_de_cartes(jeu_de_cartes, renderer);

    Carte main[7] = {0};
    tirer_quatre_cartes(main, jeu_de_cartes);

    Carte player_hand[4] = {0};  // Player's hand with 4 cards
    tirer_quatre_cartes(player_hand, jeu_de_cartes);  // Initialize player's hand

    SDL_Rect rectangles[7];
    int rect_width = 100;
    int rect_height = 150;
    int spacing = 5;
    centrer_cartes(rectangles, 4, 900, rect_width, rect_height, spacing);

    SDL_Rect middle_rect = { (900 - rect_width) / 2, (700 - rect_height) / 2 - 50, rect_width, rect_height };
    Carte middle_card = tirer_carte_au_milieu(jeu_de_cartes, main);

    SDL_Rect player_hand_rect = { (900 - rect_width) / 2, 10, rect_width, rect_height };

    int carteEnDeplacement = -1; // Indice de la carte en cours de déplacement dans la main
    SDL_Rect anciennePosition = {0}; // Stocke la position précédente de la carte déplacée

    SDL_Event event;
    SDL_bool run = SDL_TRUE;
    while (run) {
        while (SDL_PollEvent(&event)) {
            switch (event.type) {
                case SDL_QUIT:
                    run = SDL_FALSE;
                    break;
                case SDL_MOUSEBUTTONDOWN:
                    if (event.button.button == SDL_BUTTON_LEFT) {
                        int mouseX, mouseY;
                        SDL_GetMouseState(&mouseX, &mouseY);
                        if (SDL_PointInRect(&(SDL_Point){mouseX, mouseY}, &buttonRect) && !premierCoup) {
                            int emptySlot = findEmptySlot(main);

                            if (emptySlot != -1) {
                                int index;
                                do {
                                    index = rand() % 40;
                                } while (carte_presente_dans_main(jeu_de_cartes[index], main) ||
                                    (jeu_de_cartes[index].numero == middle_card.numero && strcmp(jeu_de_cartes[index].type, middle_card.type) == 0) ||
                                    (jeu_de_cartes[index].numero == lastDrawnCard.numero && strcmp(jeu_de_cartes[index].type, lastDrawnCard.type) == 0));

                                main[emptySlot] = jeu_de_cartes[index];
                                lastDrawnCard = main[emptySlot]; // Mettre à jour la dernière carte tirée

                                centrer_cartes(rectangles, 7, 900, rect_width, rect_height, spacing);
                            }
                        }

                        for (int i = 0; i < 7; ++i) {
                            if (mouseX >= rectangles[i].x && mouseX <= rectangles[i].x + rectangles[i].w &&
                                mouseY >= rectangles[i].y && mouseY <= rectangles[i].y + rectangles[i].h) {
                                carteEnDeplacement = i;
                                anciennePosition = rectangles[i];
                                break;
                            }
                        }

                        premierCoup = 0;
                    }
                    break;
                case SDL_MOUSEBUTTONUP:
                    if (event.button.button == SDL_BUTTON_LEFT && carteEnDeplacement != -1) {
                        int mouseX, mouseY;
                        SDL_GetMouseState(&mouseX, &mouseY);
                        if (mouseX >= middle_rect.x && mouseX <= middle_rect.x + middle_rect.w &&
                            mouseY >= middle_rect.y && mouseY <= middle_rect.y + middle_rect.h) {
                            if (main[carteEnDeplacement].numero == middle_card.numero || strcmp(main[carteEnDeplacement].type, middle_card.type) == 0) {
                                middle_card = main[carteEnDeplacement];
                                main[carteEnDeplacement].texture = NULL;
                            } else {
                                show_invalid_move_message(renderer);
                                rectangles[carteEnDeplacement] = anciennePosition;
                            }
                        } else {
                            rectangles[carteEnDeplacement] = anciennePosition;
                        }
                        carteEnDeplacement = -1;
                    }
                    break;
                case SDL_MOUSEMOTION:
                    if (carteEnDeplacement != -1) {
                        int mouseX, mouseY;
                        SDL_GetMouseState(&mouseX, &mouseY);
                        rectangles[carteEnDeplacement].x = mouseX - rect_width / 2;
                        rectangles[carteEnDeplacement].y = mouseY - rect_height / 2;
                    }
                    break;
                default:
                    break;
            }
        }

        SDL_RenderClear(renderer);
        animation(background_texture, fen, renderer);
        drawTransparentButton(renderer, buttonRect);
        render_rectangles(renderer, rectangles, main, &middle_rect, middle_card);
        render_player_hand(renderer, &player_hand_rect, 4);  // Render player's hidden hand
        SDL_RenderPresent(renderer);
        SDL_Delay(50);
    }

    for (int i = 0; i < 40; ++i) {
        SDL_DestroyTexture(jeu_de_cartes[i].texture);
    }
    SDL_DestroyTexture(background_texture);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(fen);
    SDL_Quit(); 

    return 0;
}