#include <SDL2/SDL.h>
#include <stdio.h>
#include <SDL2/SDL_image.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

typedef struct {
 int numero;
 const char* type;
 SDL_Texture* texture;
} Carte;

typedef struct Node {
 Carte carte;
 struct Node* parent;
 struct Node** children;
 int num_children;
 int visits;
 double wins;
} Node;

Node* create_node(Carte carte, Node* parent) {
 Node* node = (Node*)malloc(sizeof(Node));
 node->carte = carte;
 node->parent = parent;
 node->children = NULL;
 node->num_children = 0;
 node->visits = 0;
 node->wins = 0.0;
 return node;
}

int findEmptySlot(Carte* hand) {
 for (int i = 0; i < 7; ++i) {
 if (hand[i].texture == NULL) {
 return i;
 }
 }
 return -1;
}

double UCT(Node* node) {
 if (node->visits == 0) return INFINITY;
 if (!node->parent) {
 return 0;
 }
 return (node->wins / node->visits) + sqrt(2 * log(node->parent->visits) / node->visits);
}

int isTerminalState(Node* node, Carte* main) {
 int hand_is_empty = 1;
 for (int i = 0; i < node->num_children; ++i) {
 if (node->children[i] != NULL) {
 hand_is_empty = 0;
 break;
 }
 }
 return hand_is_empty;
}

Node* select_best_child(Node* node) {
 Node* best_child = NULL;
 double best_uct = -INFINITY;
 for (int i = 0; i < node->num_children; ++i) {
 double uct_value = UCT(node->children[i]);
 if (uct_value > best_uct) {
 best_uct = uct_value;
 best_child = node->children[i];
 }
 }
 return best_child;
}

void freeTree(Node* node) {
 if (node == NULL) return;
 for (int i = 0; i < node->num_children; ++i) {
 freeTree(node->children[i]);
 }
 free(node->children);
 free(node);
}

void expand_node(Node* node, Carte* main, int num_cartes_main) {
 int num_legal_moves = 0;
 for (int i = 0; i < num_cartes_main; ++i) {
 if (main[i].texture != NULL && 
 (main[i].numero == node->carte.numero || strcmp(main[i].type, node->carte.type) == 0)) {
 ++num_legal_moves;
 }
 }

 node->children = (Node**)malloc(num_legal_moves * sizeof(Node*));
 node->num_children = num_legal_moves;

 int child_index = 0;
 for (int i = 0; i < num_cartes_main; ++i) {
 if (main[i].texture != NULL && 
 (main[i].numero == node->carte.numero || strcmp(main[i].type, node->carte.type) == 0)) {
 Node* child = create_node(main[i], node);
 node->children[child_index++] = child;
 }
 }
}

double simulate(Node* node, Carte* jeu_de_cartes, int num_cartes_total) {
 Carte middle_card = node->carte;
 Carte player_hand[7] = {0};
 Carte opponent_hand[7] = {0};

 for (int i = 0; i < 4; ++i) {
 player_hand[i] = jeu_de_cartes[rand() % num_cartes_total];
 opponent_hand[i] = jeu_de_cartes[rand() % num_cartes_total];
 }

 int player_turn = 1;

 while (1) {
 if (player_turn) {
 int played = 0;
 for (int i = 0; i < 7; ++i) {
 if (player_hand[i].texture != NULL && 
 (player_hand[i].numero == middle_card.numero || strcmp(player_hand[i].type, middle_card.type) == 0)) {
 middle_card = player_hand[i];
 player_hand[i].texture = NULL;
 played = 1;
 break;
 }
 }
 if (!played) {
 int empty_slot = findEmptySlot(player_hand);
 if (empty_slot != -1) {
 player_hand[empty_slot] = jeu_de_cartes[rand() % num_cartes_total];
 }
 }

 int cards_left = 0;
 for (int i = 0; i < 7; ++i) {
 if (player_hand[i].texture != NULL) {
 cards_left++;
 }
 }
 if (cards_left == 0) {
 return 1.0;
 }
 } else {
 int played = 0;
 for (int i = 0; i < 7; ++i) {
 if (opponent_hand[i].texture != NULL && 
 (opponent_hand[i].numero == middle_card.numero || strcmp(opponent_hand[i].type, middle_card.type) == 0)) {
 middle_card = opponent_hand[i];
 opponent_hand[i].texture = NULL;
 played = 1;
 break;
 }
 }
 if (!played) {
 int empty_slot = findEmptySlot(opponent_hand);
 if (empty_slot != -1) {
 opponent_hand[empty_slot] = jeu_de_cartes[rand() % num_cartes_total];
 }
 }

 int cards_left = 0;
 for (int i = 0; i < 7; ++i) {
 if (opponent_hand[i].texture != NULL) {
 cards_left++;
 }
 }
 if (cards_left == 0) {
 return 0.0;
 }
 }

 player_turn = !player_turn;
 }
}

void backpropagate(Node* node, double result) {
 while (node != NULL) {
 node->visits += 1;
 node->wins += result;
 node = node->parent;
 }
}

Carte mcts(Carte* jeu_de_cartes, Carte* main, int num_cartes_main, Carte middle_card, int num_cartes_total, int iterations) {
 Node* root = create_node(middle_card, NULL);
 for (int i = 0; i < iterations; ++i) {
 Node* node = root;
 while (node->num_children > 0 && !isTerminalState(node, main)) {
 node = select_best_child(node);
 }
 if (!isTerminalState(node, main)) {
 expand_node(node, main, num_cartes_main);
 node = select_best_child(node);
 }
 double result = 0.0;
 int num_simulations_per_iteration = 1;
 for (int j = 0; j < num_simulations_per_iteration; ++j) {
 result += simulate(node, jeu_de_cartes, num_cartes_total);
 }
 result /= num_simulations_per_iteration;
 backpropagate(node, result);
 }
 Node* best_node = select_best_child(root);
 Carte best_move = best_node->carte;
 freeTree(root);
 return best_move;
}

SDL_Texture* load_texture_from_image(const char* file_image_name, SDL_Renderer* renderer) {
    SDL_Surface* my_image = IMG_Load(file_image_name);
    if (my_image == NULL) {
        fprintf(stderr, "Error loading image %s: %s\n", file_image_name, IMG_GetError());
        return NULL;
    }
    SDL_Texture* my_texture = SDL_CreateTextureFromSurface(renderer, my_image);
    SDL_FreeSurface(my_image);
    if (my_texture == NULL) {
        fprintf(stderr, "Error creating texture from %s: %s\n", file_image_name, SDL_GetError());
    }
    return my_texture;
}

void drawTransparentButton(SDL_Renderer* renderer, SDL_Rect rect) {
    SDL_SetRenderDrawColor(renderer, 250, 0, 0, 0); // Transparent
    SDL_RenderFillRect(renderer, &rect);
    // (Vous pouvez ajouter une icône ou du texte ici si vous le souhaitez)
}

void init_jeu_de_cartes(Carte* jeu_de_cartes, SDL_Renderer* renderer) {
    const char* types[] = {"A", "B", "C", "D"};
    char filename[50];
    int index = 0;
    for (int t = 0; t < 4; ++t) {
        for (int n = 1; n <= 7; ++n) {
            jeu_de_cartes[index].numero = n;
            jeu_de_cartes[index].type = types[t];
            sprintf(filename, "%d%s.bmp", n, types[t]);
            jeu_de_cartes[index].texture = load_texture_from_image(filename, renderer);
            ++index;
        }
        for (int n = 10; n <= 12; ++n) {
            jeu_de_cartes[index].numero = n;
            jeu_de_cartes[index].type = types[t];
            sprintf(filename, "%d%s.bmp", n, types[t]);
            jeu_de_cartes[index].texture = load_texture_from_image(filename, renderer);
            ++index;
        }
    }
}

int carte_presente_dans_main(Carte carte, Carte* main) {
    for (int i = 0; i < 7; ++i) {
        if (main[i].texture != NULL && carte.numero == main[i].numero && strcmp(carte.type, main[i].type) == 0) {
            return 1;
        }
    }
    return 0;
}

void melanger_cartes(Carte* cartes, int n) {
    srand(time(NULL));
    for (int i = 0; i < n - 1; ++i) {
        int j = i + rand() / (RAND_MAX / (n - i) + 1);
        Carte temp = cartes[i];
        cartes[i] = cartes[j];
        cartes[j] = temp;
    }
}

void tirer_quatre_cartes(Carte* main, Carte* jeu_de_cartes) {
    melanger_cartes(jeu_de_cartes, 40);
    for (int i = 0; i < 4; ++i) {
        main[i] = jeu_de_cartes[i];
    }
}

Carte tirer_carte_au_milieu(Carte* jeu_de_cartes, Carte* main) {
    int index;
    do {
        index = rand() % 40; // Choix aléatoire parmi toutes les cartes
    } while (carte_presente_dans_main(jeu_de_cartes[index], main)); // Vérifie si la carte est déjà dans la main

    return jeu_de_cartes[index];
}

void animation(SDL_Texture* text, SDL_Window* fen, SDL_Renderer* renderer) {
    SDL_Rect win = { 0 }, bg = { 0 };
    SDL_QueryTexture(text, NULL, NULL, &bg.w, &bg.h);
    SDL_GetWindowSize(fen, &win.w, &win.h);

    static int off = 0;
    if (off + 700 <= 0) {
        off = 0;
    }
    off -= 10;

    SDL_Rect rect1 = { off, 0, 800, 600 };
    SDL_Rect rect2 = { off + 800, 0, 800, 600 };
    SDL_Rect source = { 0, 0, bg.w, bg.h };

    SDL_RenderCopy(renderer, text, &source, &rect1);
    SDL_RenderCopy(renderer, text, &source, &rect2);
}

void render_rectangles(SDL_Renderer* renderer, SDL_Rect* rectangles, Carte* main, SDL_Rect* middle_rect, Carte middle_card) {
    for (int i = 0; i < 7; ++i) {
        if (main[i].texture != NULL) {
            SDL_RenderCopy(renderer, main[i].texture, NULL, &rectangles[i]);
        }
    }
    SDL_RenderCopy(renderer, middle_card.texture, NULL, middle_rect);
}

void show_invalid_move_message(SDL_Renderer* renderer) {
    SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Invalid Move", "You can only play a card with the same type or number!", NULL);
}

void centrer_cartes(SDL_Rect* rectangles, int nb_cartes, int largeur_fenetre, int rect_width, int rect_height, int spacing) {
    int total_width = nb_cartes * rect_width + (nb_cartes - 1) * 
    spacing;
    int start_x = (largeur_fenetre - total_width) / 2;
    for (int i = 0; i < nb_cartes; ++i) {
        rectangles[i].x = start_x + i * (rect_width + spacing);
        rectangles[i].y = 700 - rect_height - 110;
        rectangles[i].w = rect_width;
        rectangles[i].h = rect_height;
    }
}

void render_player_hand(SDL_Renderer* renderer, SDL_Rect* player_hand_rect, int num_cards) {
    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255); // Red color for the hidden hand rectangle
    SDL_RenderFillRect(renderer, player_hand_rect);
    
    // Display the number of cards in the player's hand
    char text[5];
    sprintf(text, "%d", num_cards);
    // Assuming you have a function to render text. For example:
    // renderText(renderer, text, player_hand_rect->x + player_hand_rect->w / 2, player_hand_rect->y + player_hand_rect->h / 2);
}

int main(int argc, char* argv[]) {

 SDL_Window* fen;
 SDL_Renderer* renderer;
 SDL_Texture* background_texture;
 SDL_Rect rectangles[7];
 Carte main_joueur[7] = { 0 };
 Carte main_ia[7] = { 0 };
 Carte jeu_de_cartes[40];
 Carte middle_card = { 0 };
 int pioche_index = 0;

 srand(time(NULL));

 if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
 fprintf(stderr, "Erreur SDL_Init : %s", SDL_GetError());
 return EXIT_FAILURE;
 }

 if (SDL_CreateWindowAndRenderer(800, 600, 0, &fen, &renderer) != 0) {
 fprintf(stderr, "Erreur SDL_CreateWindowAndRenderer : %s", SDL_GetError());
 SDL_Quit();
 return EXIT_FAILURE;
 }

 background_texture = load_texture_from_image("background.bmp", renderer);
 if (!background_texture) {
 SDL_DestroyRenderer(renderer);
 SDL_DestroyWindow(fen);
 SDL_Quit();
 return EXIT_FAILURE;
 }

 // Initialisation des cartes
 const char* types[4] = { "A", "B", "C", "D" };
 for (int i = 0; i < 4; ++i) {
 for (int j = 1; j <= 7; ++j) {
 int index = i * 10 + (j - 1);
 jeu_de_cartes[index].numero = j;
 jeu_de_cartes[index].type = types[i];
 char file_name[20];
 sprintf(file_name, "%d%s.bmp", j, types[i]);
 jeu_de_cartes[index].texture = load_texture_from_image(file_name, renderer);
 }
 
 
 }

 // Distribution des cartes initiales
 for (int i = 0; i < 7; ++i) {
 main_joueur[i] = jeu_de_cartes[rand() % 40];
 main_ia[i] = jeu_de_cartes[rand() % 40];
 }

 // Création des rectangles pour l'affichage
 for (int i = 0; i < 7; ++i) {
 rectangles[i].x = i * 100 + 50;
 rectangles[i].y = 400;
 rectangles[i].w = 100;
 rectangles[i].h = 150;
 }

 SDL_Rect middle_rect = { 350, 200, 100, 150 };

 int quit = 0;
 SDL_Event e;

 while (!quit) {
 while (SDL_PollEvent(&e)) {
 if (e.type == SDL_QUIT) {
 quit = 1;
 }
 }

 // Tour du joueur humain
 if (e.type == SDL_MOUSEBUTTONDOWN && e.button.button == SDL_BUTTON_LEFT) {
 int x, y;
 SDL_GetMouseState(&x, &y);
 for (int i = 0; i < 7; ++i) {
 if (x >= rectangles[i].x && x <= rectangles[i].x + rectangles[i].w &&
 y >= rectangles[i].y && y <= rectangles[i].y + rectangles[i].h &&
 main_joueur[i].texture != NULL &&
 (main_joueur[i].numero == middle_card.numero || strcmp(main_joueur[i].type, middle_card.type) == 0)) {
 middle_card = main_joueur[i];
 main_joueur[i].texture = NULL;
 break;
 }
 }

 // Vérification si la partie est terminée après le tour du joueur
 if (isTerminalState(NULL, main_joueur)) {
 printf("Le joueur a gagné !\n");
 quit = 1;
 }
 }

 // Tour de l'IA
 if (!quit) {
 Carte ia_move = mcts(jeu_de_cartes, main_ia, 7, middle_card, 40, 10000);

 for (int i = 0; i < 7; ++i) {
 if (main_ia[i].texture != NULL && 
 (main_ia[i].numero == ia_move.numero || strcmp(main_ia[i].type, ia_move.type) == 0)) {
 middle_card = main_ia[i];
 main_ia[i].texture = NULL;
 break;
 }
 }

 // Vérification si la partie est terminée après le tour de l'IA
 if (isTerminalState(NULL, main_ia)) {
 printf("L'IA a gagné !\n");
 quit = 1;
 }
 }

 // Affichage de l'interface utilisateur
 SDL_RenderClear(renderer);
 animation(background_texture, fen, renderer);
 render_rectangles(renderer, rectangles, main_joueur, &middle_rect, middle_card);
 SDL_RenderPresent(renderer);
 }

 // Libération des ressources
 SDL_DestroyTexture(background_texture);
 for (int i = 0; i < 40; ++i) {
 SDL_DestroyTexture(jeu_de_cartes[i].texture);
 }
 SDL_DestroyRenderer(renderer);
 SDL_DestroyWindow(fen);
 SDL_Quit();
 return EXIT_SUCCESS;
}
